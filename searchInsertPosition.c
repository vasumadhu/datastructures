/* C Program to check the position of a target number in an sorted array.
If the target number does not exist then the position where the target can be inserted should be retured */

#include<stdio.h>

int searchPosition(int array[],int n,int target)
{
	int output =0;
	int L=0, R = n-1;
	while(L<R)
	{
		int M = (L+R)/2;
		if(array[M]<target)
		{
			L = M+1;
		}
		else
			R =M;
	}
	
	if(array[L] < target)
		output= L+1;
	else
		output = L;

	return output;
	
}
int main()
{
	int n =  5;
	int array[5] = {1,3,4,6,7};
	int target = -1;
	int result = searchPosition(array,n,target);
	printf("%d",result+1);
	return 0;
}