/* Find the minimum in a rotated sorted array 
Solution: o(log n) */

#include<stdio.h>

int findMin(int arr[],int n)
{
	int L=0;
	int R = n-1;
	while(L<R && arr[L] >= arr[R])
	{
		int M = (L+R)/2;
		if(arr[M] > arr[R])
			L=M+1;
		else
			R=M;
	}
	return arr[L];
	
}

int main()
{
	int n = 5;
	int arr[5]={4,5,6,2,3};
	int result = 0;
	result = findMin(arr,n);
	printf("%d",result);
	return 0;
}